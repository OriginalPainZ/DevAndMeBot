package com.devandme.devandmebot.core;

import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.commands.PowerRegistry;
import com.devandme.devandmebot.listeners.BotListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class DevAndMe {

    private static final Logger LOGGER = LoggerFactory.getLogger(DevAndMe.class);

    public static Gson GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

    private final ScheduledExecutorService executorService;

    private final JDA jda;

    private final Configuration configuration;

    private final CommandMap commandMap;

    private volatile boolean running = true;

    public DevAndMe() throws Exception {
        LOGGER.info("Démarrage de DevAndMeBot...");

        this.configuration = Configuration.load();

        this.executorService = Executors.newScheduledThreadPool(1, new DaemonThreadFactory());

        this.commandMap = new CommandMap(this);

        jda = new JDABuilder(AccountType.BOT).setToken(this.configuration.getDiscordToken()).buildAsync();
        jda.addEventListener(new BotListener(commandMap));
        
        this.executorService.scheduleAtFixedRate(
                new GameSwitcher(this.configuration.getGames(), this.jda),
                0,
                this.configuration.getGameRotationDelay(),
                this.configuration.getGameRotationDelayUnit()
        );

        LOGGER.info("Bot démarré!");
    }

    public JDA getJda() {
        return jda;
    }

    public void loop() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (running) {
                commandMap.commandConsole(scanner.nextLine());
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void stop() {
        this.running = false;

        this.jda.shutdown();
        new DataRegistery().saveMessagesCount();
        PowerRegistry.getInstance().save();
    }

    @Override
    protected void finalize() throws Throwable {
        this.executorService.shutdown();
    }
}