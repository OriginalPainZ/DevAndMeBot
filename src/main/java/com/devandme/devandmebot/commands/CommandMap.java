package com.devandme.devandmebot.commands;

import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.commands.defaults.CommandDefault;
import com.devandme.devandmebot.commands.defaults.PowerCommand;
import com.devandme.devandmebot.commands.defaults.StatsCommand;
import com.devandme.devandmebot.core.DevAndMe;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class CommandMap {

    @SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandMap.class);

	private final Map<String, SimpleCommand> commands = new HashMap<>();
	private final JDA jda;

    public CommandMap(DevAndMe devAndMe){
		this.jda = devAndMe.getJda();

		this.registerCommands(new CommandDefault(devAndMe), new PowerCommand(this), new StatsCommand());
		
		PowerRegistry.getInstance().load();
	}

	public int getPowerUser(Guild guild, User user){
		if(guild.getMember(user).hasPermission(Permission.ADMINISTRATOR)) return 100;

		return PowerRegistry.getInstance().getPower(user.getIdLong()) != null ? PowerRegistry.getInstance().getPower(user.getIdLong()) : 0;
	}

	public void setUserPower(User user, int power){
		if(power == 0) removeUserPower(user);
		else PowerRegistry.getInstance().setPower(user.getIdLong(), power);
	}

	public void removeUserPower(User user){
		PowerRegistry.getInstance().remove(user.getIdLong());
	}

    public String getTag(){
        return "!";
    }

    public Collection<SimpleCommand> getCommands(){
        return commands.values();
    }

    public void registerCommands(Object...objects){
        for(Object object : objects) registerCommand(object);
    }

    public void registerCommand(Object object){
        for(Method method : object.getClass().getDeclaredMethods()){
            if(method.isAnnotationPresent(Command.class)){
                Command command = method.getAnnotation(Command.class);
                method.setAccessible(true);
                SimpleCommand simpleCommand = new SimpleCommand(command.name(), command.description(), command.power(), command.type(), object, method);
                commands.put(command.name(), simpleCommand);
            }
        }
    }

    public void commandConsole(String command){
        Object[] object = getCommand(command);
        if(object[0] == null || ((SimpleCommand)object[0]).getType() == ExecutorType.USER){
            System.out.println("Commande inconnue.");
            return;
        }
        try{
            execute(((SimpleCommand)object[0]), command, (String[])object[1], null);
        }catch(Exception exception){
            System.out.println("La methode "+((SimpleCommand)object[0]).getMethod().getName()+" n'est pas correctement initialisé.");
            exception.printStackTrace();
        }
    }

    public boolean commandUser(User user, String command, Message message){
        Object[] object = getCommand(command);
        if(object[0] == null || ((SimpleCommand)object[0]).getType() == ExecutorType.CONSOLE) return false;

        if(message.getGuild() != null && ((SimpleCommand)object[0]).getPower() > getPowerUser(message.getGuild(), message.getAuthor())) return false;

        try{
            execute(((SimpleCommand)object[0]), command,(String[])object[1], message);
        }catch(Exception exception){
            System.out.println("La methode "+((SimpleCommand)object[0]).getMethod().getName()+" n'est pas correctement initialisé.");
            exception.printStackTrace();
        }
        return true;
    }

    private Object[] getCommand(String command){
        String[] commandSplit = command.split(" ");
        String[] args = new String[commandSplit.length-1];
        for(int i = 1; i < commandSplit.length; i++) args[i-1] = commandSplit[i];
        SimpleCommand simpleCommand = commands.get(commandSplit[0]);
        return new Object[]{simpleCommand, args};
    }

    private void execute(SimpleCommand simpleCommand, String command, String[] args, Message message) throws Exception{
        Parameter[] parameters = simpleCommand.getMethod().getParameters();
        Object[] objects = new Object[parameters.length];

        for(int i = 0; i < parameters.length; i++){
            if(parameters[i].getType() == String[].class) objects[i] = args;
            else if(parameters[i].getType() == User.class) objects[i] = message == null ? null : message.getAuthor();
            else if(parameters[i].getType() == TextChannel.class) objects[i] = message == null ? null : message.getTextChannel();
            else if(parameters[i].getType() == PrivateChannel.class) objects[i] = message == null ? null : message.getPrivateChannel();
            else if(parameters[i].getType() == Guild.class) objects[i] = message == null ? null : message.getGuild();
            else if(parameters[i].getType() == String.class) objects[i] = command;
            else if(parameters[i].getType() == Message.class) objects[i] = message;
            else if(parameters[i].getType() == JDA.class) objects[i] = this.jda;
            else if(parameters[i].getType() == MessageChannel.class) objects[i] = message == null ? null : message.getChannel();
        }
        simpleCommand.getMethod().invoke(simpleCommand.getObject(), objects);
    }
}