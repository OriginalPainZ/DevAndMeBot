package com.devandme.devandmebot.commands.defaults;

import java.awt.Color;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.core.DataRegistery;
import com.devandme.devandmebot.utils.Utils;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class StatsCommand {
	
	@Command(name="stats")
	private void power(User user, MessageChannel channel, Message message, String[] args){
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setTitle("Statistiques :");
		embedBuilder.setColor(Color.yellow);
		embedBuilder.addField("Messages totaux :", Utils.formatInteger(new DataRegistery().getAllMessage()), true);
		embedBuilder.addField("Messages cette session :", Utils.formatInteger(new DataRegistery().getSessionMessages()), true);
		
		channel.sendMessage(embedBuilder.build()).queue();
	}
	
}
